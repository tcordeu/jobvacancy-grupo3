require 'spec_helper'

describe JobOffer do

  describe 'model' do

    subject { @job_offer = JobOffer.new }

    it { should respond_to( :id) }
    it { should respond_to( :title ) }
    it { should respond_to( :location) }
    it { should respond_to( :description ) }
    it { should respond_to( :owner ) }
    it { should respond_to( :owner= ) }
    it { should respond_to( :created_on) }
    it { should respond_to( :updated_on ) }
    it { should respond_to( :is_active) }
    it { should respond_to( :application_count) }
    it { should respond_to( :experience) }
    it { should respond_to( :start_date) }

  end

  describe 'is_active' do
    it 'should be inactive after 31 days' do
      jo = JobOffer.new
      jo.start_date = Date.today
      jo.update_is_active(Date.today + 31)
      jo.is_active.should be false
    end
  end


  describe 'valid?' do

    let(:job_offer) do
      user = User.new
      user.id = 1
      job_offer = JobOffer.new
      job_offer.owner = user
      job_offer
    end

    it 'should be false when title is blank' do
      expect(job_offer.valid?).to eq false
    end

    it 'should be true when title is not blank' do
      job_offer.title = 'the title'
      expect(job_offer.valid?).to eq true
    end

  end

  describe 'experience?' do

    let(:job_offer) do
      user = User.new
      user.id = 1
      job_offer = JobOffer.new
      job_offer.owner = user
      job_offer.title = 'the title'
      job_offer
    end

    it 'should be false when experience is not a number' do
      job_offer.experience = 'test'
      expect(job_offer.experience?).to eq false
    end

    it 'should be false when experience is ""' do
      job_offer.experience = ''
      expect(job_offer.experience?).to eq false
    end

    it 'should be false when experience is -8' do
      job_offer.experience = -8
      expect(job_offer.experience?).to eq false
    end

    it 'should be true when experience is 0' do
      job_offer.experience = 0
      expect(job_offer.experience?).to eq true
    end

    it 'should be true when experience is 5' do
      job_offer.experience = 5
      expect(job_offer.experience?).to eq true
    end

  end

end
