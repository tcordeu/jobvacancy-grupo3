require 'spec_helper'

describe JobApplication do

  describe 'model' do

    subject { @job_offer = JobApplication.new }

    it { should respond_to( :applicant_email ) }
    it { should respond_to( :job_offer) }

  end


  describe 'create_for' do

    it 'should set applicant_email' do
      email = 'applicant@test.com'
      ja = JobApplication.create_for(email,'http://www.applicant_cv.com/', JobOffer.new)
      ja.applicant_email.should eq email
    end

    it 'should not set invalid applicant_email' do
      email = 'applicant@test'
      ja = JobApplication.create_for(email,'http://www.applicant_cv.com/', JobOffer.new)
      ja.applicant_email.should eq nil
    end

    it 'should set applicant_cv' do
      cv = 'http://www.applicant_cv.com/'
      ja = JobApplication.create_for('applicant@test.com',cv, JobOffer.new)
      ja.cv_link.should eq cv
    end

    it 'should not set invalid applicant_cv' do
      cv = 'ww.applicant_cv.com'
      ja = JobApplication.create_for('applicant@test.com',cv, JobOffer.new)
      ja.cv_link.should eq nil
    end

    it 'should set job_offer' do
      offer = JobOffer.new
      ja = JobApplication.create_for('applicant@test.com','http://www.applicant_cv.com/', offer)
      ja.job_offer.should eq offer
    end

    it 'should increment job_offer application counter' do
      offer = JobOffer.new
      ja = JobApplication.create_for('applicant@test.com','http://www.applicant_cv.com/', offer)
      ja.increment_app_count
      ja.job_offer.application_count.should eq 1
    end

  end


  describe 'process' do

    let(:job_application) { JobApplication.new }

    it 'should deliver contact info notification' do
      ja = JobApplication.create_for('applicant@test.com','http://www.applicant_cv.com/',JobOffer.new)
      JobVacancy::App.should_receive(:deliver).with(:notification, :contact_info_email, ja)
      ja.process
    end

  end

end
