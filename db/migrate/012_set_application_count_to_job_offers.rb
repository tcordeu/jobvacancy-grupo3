migration 12, :set_application_count_to_job_offers do
  up do
    offers = JobOffer.all
    offers.each do | offer |
      offer.application_count = 0
      offer.save
    end
  end

  down do
  end
end
