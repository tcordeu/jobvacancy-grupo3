migration 14, :set_experience_field_to_job_offers do
  up do
    offers = JobOffer.all
    offers.each do | offer |
      offer.experience = -1
      offer.save
    end
  end

  down do
  end
end
