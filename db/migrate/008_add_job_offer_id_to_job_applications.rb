migration 8, :add_job_offer_id_to_job_applications do
  up do
    modify_table :job_applications do
      add_column :job_offer_id, Integer
    end
  end

  down do
    modify_table :job_applications do
      drop_column :job_offer_id
    end
  end
end
