migration 3, :create_job_applications do
  up do
    create_table :job_applications do
      column :id, Integer, :serial => true
      column :applicant_email, DataMapper::Property::String, :length => 255
      column :cv_link, DataMapper::Property::String, :length => 255
    end
  end

  down do
    drop_table :job_applications
  end
end
