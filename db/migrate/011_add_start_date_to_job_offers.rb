migration 11, :add_start_date_to_job_offers do
  up do
    modify_table :job_offers do
      add_column :start_date, Date
    end
  end

  down do
    modify_table :job_offers do
      drop_column :start_date
    end
  end
end
