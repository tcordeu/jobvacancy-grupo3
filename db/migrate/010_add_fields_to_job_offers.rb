migration 10, :add_fields_to_job_offers do
  up do
    modify_table :job_offers do
      add_column :experience, Integer
    end
  end

  down do
    modify_table :job_offers do
      drop_column :experience
    end
  end
end
