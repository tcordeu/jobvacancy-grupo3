migration 7, :add_application_count_to_job_offers do
  up do
    modify_table :job_offers do
      add_column :application_count, Integer
    end
  end

  down do
    modify_table :job_offers do
      drop_column :application_count
    end
  end
end
