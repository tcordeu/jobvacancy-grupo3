require 'date'

class JobOffer
  include DataMapper::Resource

  property :id, Serial
  property :title, String
  property :location, String
  property :description, String
  property :created_on, Date
  property :updated_on, Date
  property :is_active, Boolean, :default => true
  property :application_count, Integer, :default => 0
  property :experience, Integer
  property :start_date, Date
  belongs_to :user
  has n, :job_applications, :constraint => :destroy!

  validates_presence_of :title

  def update_is_active(date = Date.today)
    self.is_active = (date <= (start_date + 30))
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def self.all_active
    JobOffer.all(:is_active => true)
  end

  def self.find_by_owner(user)
    JobOffer.all(:user => user)
  end

  def self.update_all_offers
    offers = JobOffer.all_active
    offers.each do |offer| 
      offer.update_is_active()
      offer.save
    end
  end

  def experience?
    (self.experience.is_a? Integer) && (self.experience > -1)
  end

  def check_and_save
    if !self.experience?
      self.experience = -1
    end
    self.save
  end

end
