require 'uri'

class JobApplication

  include DataMapper::Resource

  property :id, Serial
  property :applicant_email, String
  property :cv_link, String
  
  belongs_to :job_offer

  validates_presence_of :applicant_email
  validates_presence_of :cv_link

  attr_accessor :job_offer

  def self.create_for(email, cv, offer)
    app = JobApplication.new
    valid_email_regex = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    if !(email =~ valid_email_regex)
      return app
    end
    if !(cv =~ URI::regexp)
      return app
    end
    app.applicant_email = email
    app.cv_link = cv
    app.job_offer = offer
    app
  end

  def process
    JobVacancy::App.deliver(:notification, :contact_info_email, self)
  end

  def self.find_by_offer(offer)
    JobApplication.all(:job_offer => offer)
  end

  def increment_app_count
    job_offer.application_count += 1
    job_offer.save
  end

end
