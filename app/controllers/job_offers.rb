require 'date'

JobVacancy::App.controllers :job_offers do
  
  get :my do
    JobOffer.update_all_offers
    @offers = JobOffer.find_by_owner(current_user)
    render 'job_offers/my_offers'
  end    

  get :index do
    JobOffer.update_all_offers
    @offers = JobOffer.all_active
    render 'job_offers/list'
  end  

  get :new do
    @job_offer = JobOffer.new
    render 'job_offers/new'
  end

  get :latest do
    JobOffer.update_all_offers
    @offers = JobOffer.all_active
    render 'job_offers/list'
  end

  get :edit, :with =>:offer_id  do
    @job_offer = JobOffer.get(params[:offer_id])
    # ToDo: validate the current user is the owner of the offer
    render 'job_offers/edit'
  end

  get :apply, :with =>:offer_id  do
    @job_offer = JobOffer.get(params[:offer_id])
    @job_application = JobApplication.new
    # ToDo: validate the current user is the owner of the offer
    render 'job_offers/apply'
  end

  post :search do
    @offers = JobOffer.all(:title.like => "%#{params[:q]}%")
    render 'job_offers/list'
  end


  post :apply, :with => :offer_id do
    @job_offer = JobOffer.get(params[:offer_id])    
    applicant_email = params[:job_application][:applicant_email]
    cv_link = params[:job_application][:cv_link]
    @job_application = JobApplication.create_for(applicant_email, cv_link, @job_offer)
    if @job_application.save
      @job_application.increment_app_count
      @job_application.process
      flash[:success] = 'Contact information sent.'
      redirect '/job_offers'
    else
      flash.now[:error] = 'Operation failed'
      redirect '/job_offers/my'
    end

  end

  post :create do
    @job_offer = JobOffer.new(params[:job_offer])
    @job_offer.start_date = Date.today
    @job_offer.owner = current_user
    if @job_offer.check_and_save
      flash[:success] = 'Offer created'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/new'
    end  
  end

  post :update, :with => :offer_id do
    @job_offer = JobOffer.get(params[:offer_id])
    @job_offer.update(params[:job_offer])
    if @job_offer.check_and_save
      flash[:success] = 'Offer updated'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/edit'
    end  
  end

  put :activate, :with => :offer_id do
    @job_offer = JobOffer.get(params[:offer_id])
    @job_offer.start_date = Date.today
    @job_offer.update_is_active
    if @job_offer.check_and_save
      flash[:success] = 'Offer activated'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Operation failed'
      redirect '/job_offers/my'
    end  
  end

  delete :destroy do
    @job_offer = JobOffer.get(params[:offer_id])
    if @job_offer.destroy
      flash[:success] = 'Offer deleted'
    else
      flash.now[:error] = 'Title is mandatory'
    end
    redirect 'job_offers/my'
  end

  get :view_applications, :with =>:offer_id  do
    @job_offer = JobOffer.get(params[:offer_id])
    @job_applications = JobApplication.find_by_offer(@job_offer)
    # ToDo: validate the current user is the owner of the offer
    render 'job_offers/view_applications'
  end

end
