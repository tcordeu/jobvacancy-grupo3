Feature: Job Offer

  Scenario: deactivated offer is in my offers list
    Given the job offer "Developer Java Ssr" that was created on 10/4/2018
    When I log in to the app on 10/5/2018
    And I get the MyOffers list
    Then the list only contains that offer
  
  Scenario: offer after more than 30 days is inactive
    Given the job offer "Developer Java Ssr" that was created on 10/4/2018
    When I log in to the app on 11/5/2018
    Then the offer is inactive

  Scenario: offer after exactly 30 days is active
    Given the job offer "Developer Java Ssr" that was created on 10/4/2018
    When I log in to the app on 10/5/2018
    Then the offer is active

  Scenario: deactivated offer isnt in job offers list
    Given the job offer "Developer Java Ssr" that was created on 10/4/2018
    When I log in to the app on 11/5/2018
    And I get the JobOffers list
    Then the list is empty

  Scenario: active offer is in job offers list
    Given the job offer "Developer Java Ssr" that was created on 10/4/2018
    When I log in to the app on 10/5/2018
    And I get the JobOffers list
    Then the list only contains that offer



  