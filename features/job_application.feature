Feature: Job Application
  In order to get a job
  As a candidate
  I want to apply to an offer

  Background:
    Given only a "Web Programmer" offer exists in the offers list

  Scenario: Send mail when applying to job offer
    Given I access the offers list page
    When I apply with email "applicant@test.com" and cv link "http://www.applicant_cv.com/"
    Then I should receive a mail with offerer info

  Scenario: Apply to job offer
    Given I access the offers list page
    When I apply with email "applicant@test.com" and cv link "http://www.applicant_cv.com/"
    Then the application was saved 

  Scenario: Apply to job offer - wrong cv link
    Given I access the offers list page
    When I apply with email "applicant@test.com" and cv link "http://ww.applicant_cv.com/"
    Then the application was not saved 

  Scenario: Apply to job offer - wrong email
    Given I access the offers list page
    When I apply with email "applicant@test" and cv link "http://www.applicant_cv.com/"
    Then the application was not saved 

