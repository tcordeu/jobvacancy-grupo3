Feature: Job Offer Experience
  Scenario: 7 Years of Experience
    Given I am logged in as job offerer
    Given I access the new offer page
    When I fill the title with "Developer Java"
    And I fill the experience with "7"
    And confirm the new offer
    Then I should see "Developer Java" in My Offers
    And I should see "7" in My Offers

  Scenario: No Experience Indicated
    Given I am logged in as job offerer
    Given I access the new offer page
    When I fill the title with "Developer Java"
    And I fill the experience with ""
    And confirm the new offer
    Then I should see "Developer Java" in My Offers
    And I should see "No experience indicated." in My Offers

  Scenario: Invalid Experience Indicated
    Given I am logged in as job offerer
    Given I access the new offer page
    When I fill the title with "Developer Java"
    And I fill the experience with "-10"
    And confirm the new offer
    Then I should see "Developer Java" in My Offers
    And I should see "No experience indicated." in My Offers
