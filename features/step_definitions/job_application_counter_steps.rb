Given(/^the JobVacancy user "(.*?)"$/) do |user_name|
  @user = User.new
  @user.name = user_name
end

When(/^he has a job offer named "(.*?)"$/) do |job_name|
  @job_offer = JobOffer.new
  @job_offer.title = job_name
	@job_offer.owner = @user
end

When(/^recieved (\d+) applications$/) do |no_applications|
  (1..no_applications.to_i).step(1) do |i|
    applicant = 'applicant@test.com'
    cv_link = 'http://www.applicant_cv.com/' 
    application = JobApplication.create_for(applicant,cv_link, @job_offer)
    application.increment_app_count
  end
end

Then(/^"(.*?)" can check on his Job Offers list that de "(.*?)" post has (\d+) applications\.$/) do |user_name, job_name, no_applications|
  expect(@job_offer.title).to eq job_name
  expect(@job_offer.application_count).to eq no_applications.to_i
  expect(@job_offer.owner.name).to eq user_name
end
