require 'date'

Given(/^the job offer "(.*?)" that was created on (\d+)\/(\d+)\/(\d+)$/) do |job_name, day, month, year|
  JobOffer.all.destroy
  @offer = JobOffer.new
  @offer.start_date = Date.new(year.to_i,month.to_i,day.to_i)
  @offer.owner = User.first
  @offer.title = job_name
  @offer.location = 'a nice job'
  @offer.description = 'a nice job'
  @offer.save
end

When(/^I log in to the app on (\d+)\/(\d+)\/(\d+)$/) do |day, month, year|
  @offer.update_is_active(Date.new(year.to_i,month.to_i,day.to_i))
  @offer.save
end

Then(/^the offer is inactive$/) do
  expect(@offer.is_active).to eq false
end

Then(/^the offer is active$/) do
  expect(@offer.is_active).to eq true
end

Given(/^that there is only one job offer and it is inactive$/) do
  @offer = JobOffer.new
  @offer.is_active = false
end

Given(/^than only exists the inactive offer "(.*?)"$/) do |job_name|
  @offer = JobOffer.new
  @offer.title = job_name
end

When(/^I get the MyOffers list$/) do
  @offers = JobOffer.all
end

Then(/^the list only contains that offer$/) do
  expect(@offers.size).to eq 1
  expect(@offers[0]).to eq @offer
end

When(/^I get the JobOffers list$/) do
  @offers = JobOffer.all_active
end

Then(/^the list is empty$/) do
  expect(@offers.size).to eq 0
end




