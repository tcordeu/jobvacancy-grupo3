require 'date'

Given(/^only a "(.*?)" offer exists in the offers list$/) do | job_title |
  @job_offer = JobOffer.new
  @job_offer.start_date = Date.today
  @job_offer.owner = User.first
  @job_offer.title = job_title
  @job_offer.location = 'a nice job'
  @job_offer.description = 'a nice job'
  @job_offer.save

end

Given(/^I access the offers list page$/) do
  visit '/job_offers'
end

When(/^I apply with email "(.*?)" and cv link "(.*?)"$/) do |email, cv_link|
  @email = email
  @cv_link = cv_link
  click_link('Apply')
  fill_in('job_application[applicant_email]', :with => email)
  fill_in('job_application[cv_link]', :with => cv_link)
  click_button('Apply')
end

Then(/^I should receive a mail with offerer info$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", "r")
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_offer.owner.name).should be true
  @job_offer.destroy
end

Then(/^the application was saved$/) do
  applications = JobApplication.find_by_offer(@job_offer)
  expect(applications.size).to eq 1
  expect(applications[0].applicant_email).to eq @email
  expect(applications[0].cv_link).to eq @cv_link
  @job_offer.destroy
end

Then(/^the application was not saved$/) do
  applications = JobApplication.find_by_offer(@job_offer)
  @job_offer.destroy
  expect(applications.size).to eq 0
end
