When(/^specifies "(.*?)" year of experience$/) do |exp|
  @job_offer.experience = exp
end

Then(/^"(.*?)" can check on his Job Offers list that the "(.*?)" post has "(.*?)" year of experience$/) do |user, job_name, exp|
  expect(@job_offer.experience).to eq exp.to_i
  expect(@job_offer.title).to eq job_name
  expect(@job_offer.owner.name).to eq user
end

When(/^I fill the experience with "(.*?)"$/) do |exp|
  fill_in('job_offer[experience]', :with => exp)
end
